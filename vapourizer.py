#!/usr/bin/python3

# Copyright 2016 Albert Aparicio
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys

# Set normal and wide character arrays
normal = (
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
    'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
    'U', 'V', 'W', 'X', 'Y', 'Z', ' ', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '!', '#', '$', '%', '&', '\'',
    '(', ')','*', '+', '-', '.', ',', '/', ':', ';', '<', '>', '@', '=', '?', '[', ']', '{', '}', '~', '|', '_'
)
changed = (
    "ａ", "ｂ", "ｃ", "ｄ", "ｅ", "ｆ", "ｇ", "ｈ", "ｉ", "ｊ", "ｋ", "ｌ", "ｍ", "ｎ", "ｏ", "ｐ", "ｑ", "ｒ", "ｓ", "ｔ",
    "ｕ", "ｖ", "ｗ", "ｘ", "ｙ", "ｚ", "Ａ", "Ｂ", "Ｃ", "Ｄ", "Ｅ", "Ｆ", "Ｇ", "Ｈ", "Ｉ", "Ｊ", "Ｋ", "Ｌ", "Ｍ", "Ｎ",
    "Ｏ", "Ｐ", "Ｑ", "Ｒ", "Ｓ", "Ｔ", "Ｕ", "Ｖ", "Ｗ", "Ｘ", "Ｙ", "Ｚ", "\x20", "０", "１", "２", "３", "４", "５", "６",
    "７", "８", "９", "！", "＃", "＄", "％", "＆", "＇", "（", "）", "＊", "＋", "－", "．", "，", "／", "：", "；", "＜",
    "＞", "＠", "＝", "？", "［", "］", "｛", "｝", "～", "｜", "＿"
)

# Check that an argument has been passed into the function
if len(sys.argv) > 1:
    for i in range(1, len(sys.argv), 1):
        # Fix so first argument is not used
        x = sys.argv[i]

        assert len(normal) == len(changed)

        # Replace characters in x from 'normal' with characters in 'changed'
        for i in range(len(normal)):
            x = x.replace(normal[i], changed[i])

        # Print out the result
        print(x)
else:
    print('Please, input at least one string to be widened.')
